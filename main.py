import os, hashlib, time
from datetime import datetime as dt
from flask import *
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.secret_key = ",\xa6\xb3\x81%\xd81Z|\xb4^'\xae\xc9N\xfc\x96\xed\x00\xbf\x96_\x97\x06"

#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///sample.db'
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']

db = SQLAlchemy(app)

import models

@app.route('/')
def home():
	# Check if a user's session exists and redirect to login if not
	user = before_request()
	if not user:
		return redirect(url_for('login'))

	# Get a list of people, this user follows
	followees = models.Follower.query.filter(models.Follower.who == user.id).all()
	followee_ids = [f.whom for f in followees]
	followee_ids.append(user.id)
	
	# Get a list of the latest tweets
	squeaks_list = models.Squeak.query.filter(models.Squeak.poster.in_(followee_ids)).order_by(models.Squeak.timestamp.desc()).all()
	
	# Prepare the tweet list object, fetching poster's name and rendering proper timestamp
	our_squeaks = []
	for squeak in squeaks_list:
		our_squeaks.append({'poster':get_user_byid(squeak.poster).name, \
							'text':squeak.text,
							'uid':squeak.poster,
							'favs':squeak.favs,
							'timestamp':get_time(squeak.timestamp)})

	# Render the template
	if our_squeaks:
		return render_template('home.html', squeaks=our_squeaks, user_hash=get_user_hash(user))
	else:
		return render_template('home.html', error_msg='No squeaks to show', user_hash=get_user_hash(user))


@app.route('/new', methods=['POST'])
def new_squeak():
	user = before_request()
	if not user:
		return redirect(url_for('login'))

	# Get squeak contents
	squeak_text = request.form['squeak_text'].strip()
	user_hash = request.form['user_hash'].strip()

	# Compare hash with that of session user
	if squeak_text == '' or user_hash != get_user_hash(user):
		return '{"result":"fail"}'

	squeak = models.Squeak(user.id, squeak_text, 0, time.time())
	db.session.add(squeak)
	db.session.commit()

	return '{"result":"pass", "reload":true}'


@app.route('/users')
def users():
	user = before_request()
	if not user:
		return redirect(url_for('login'))

	# Get users that current user follows
	user_follows = [f.whom for f in models.Follower.query.filter(models.Follower.who == user.id).all()]
	user_follows.append(user.id)

	# Get Users list
	users_list = models.Users.query.filter(~models.Users.id.in_(user_follows)).all()

	# Prepare users list
	users = []
	for user in users_list:
		users.append({'name':user.name, 'id':user.id})

	if users:
		return render_template('users.html', users=users)
	else:
		return render_template('users.html', success_msg='No more users to follow')


@app.route('/register', methods=['GET', 'POST'])
def register():
	if request.method == 'GET':
		return render_template('register.html')

	else:
		# Get form data
		name = request.form['name'].strip()
		email = request.form['email'].strip()
		password = request.form['password'].strip()
		cpassword = request.form['cpassword'].strip()

		# Validate the data
		if name == '' or email == '' or password == '' or password != cpassword:
			return render_template('register.html', error_msg='Please fill in the details properly')

		# Put in the db
		try:
			user = get_user(email)
			if user:
				flash('Account already exists, please login to continue')
				return redirect(url_for('login'))
		except Exception:
			pass

		# Actually put the user in the db
		user = models.Users(name, email, get_hashed_password(password), 1)
		db.session.add(user)
		db.session.commit()	

		flash('Registration successful, please login to conitnue')
		return redirect(url_for('login'))


@app.route('/user/<int:uid>')
def user(uid):
	user = before_request()
	if not user:
		return redirect(url_for('login'))

	# Get user specified by id
	try:
		user = get_user_byid(uid)
		if not user:
			return render_template('user.html', error_msg='No such user found')
	except Exception:
		return render_template('user.html', error_msg='No such user found')
	
	squeaks_list = models.Squeak.query.filter(models.Squeak.poster == user.id).order_by(models.Squeak.timestamp.desc()).all()

	squeaks = []
	for squeak in squeaks_list:
		squeaks.append({'text':squeak.text, \
						'favs':squeak.favs, \
						'timestamp':dt.fromtimestamp(squeak.timestamp).strftime("%d %b %Y %I:%M%p"), \
						'poster':user.name})

	return render_template('user.html', user_info=user, squeaks=squeaks)


@app.route('/follow_user', methods=['POST'])
def follow_user():
	user = before_request()
	if not user:
		return redirect(url_for('login'))

	uid = int(request.form['user_id'].strip())

	if models.Follower.query.filter(models.Follower.who == user.id).filter(models.Follower.whom == uid).all():
		return '{"result":"fail"}'

	f = models.Follower(user.id, uid)
	db.session.add(f)
	db.session.commit()
	return '{"result":"pass"}'


@app.route('/login', methods=['GET', 'POST'])
def login():
	user = before_request()
	if user:
		return redirect(url_for('home'))

	if request.method == 'GET':
		return render_template('login.html')

	else:
		# GEt the form data
		email = request.form['email'].strip()
		password = request.form['password'].strip()

		# Verify the form data
		if email == '' or password == '':
			return render_template('login.html', error_msg='Please fill in the details correctly')

		# Check if user exists
		try:
			user = get_user(email)
			if user and user.password == get_hashed_password(password):
				session['user_email'] = email
				return redirect(url_for('home'))

		except Exception:
			pass

		return render_template('login.html', error_msg='Incorrect email or password')

@app.route('/logout')
def logout():
	if 'user_email' in session:
		session.pop('user_email', None)
	return redirect(url_for('login'))


def get_hashed_password(pwd):
	return hashlib.sha1(pwd).hexdigest()

def before_request():
	user = None
	if logged_in():
		email = session['user_email']
		user = get_user(email)
	return user

def get_user(email):
	return models.Users.query.filter(models.Users.email == email).one()

def get_user_byid(uid):
	return models.Users.query.filter(models.Users.id == uid).one()

def get_user_hash(user):
	return get_hashed_password(user.email + str(user.id))

def logged_in():
	return 'user_email' in session

def get_time(timestamp):
	return dt.fromtimestamp(timestamp).strftime("%d %b %y %I:%M%p")

if __name__ == "__main__":
	#app.debug = True
	app.run('0.0.0.0', 5000)